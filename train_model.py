import modelCNN
import numpy as np
import torch

def train_model(img_data_root, device, n_epochs,save_load_path,hidden_units=4096, lr=0.001, batch_size=64, resume_training=False):
    '''input
       output
            Returns the trained model

    '''
    #load the dataset from image root folder
    data, loader = modelCNN.load_data(img_data_root, batch_size)

    if resume_training: #training on pretrained model(resume training)

        PDNet, optimizer, criterion, load_epoch = modelCNN.load_model(save_load_path, device)
        print(f'\nResume Training')
        print(f'Epochs already trained: {load_epoch}\n')

    else: #train on a new model
        #initialize the model with appropriate class output size
        PDNet = modelCNN.model_architecture(len(data['train'].classes), hidden_units)

        #initialize the Loss function and optimizer
        optimizer, criterion = modelCNN.initiate_optimizer_loss(PDNet.fc.parameters(), lr)

    # print(PDNet) # uncomment this if you want to see the model infrastucture
    PDNet = PDNet.to(device) # send to gpu or cpu

    print(f'\nCommencing Training for {n_epochs} epochs, with {lr} learning rate\n')
    ###################
    # train the model #
    ###################

    valid_loss_min = np.Inf # initialize tracker for minimum validation loss

    for epoch in range(1, n_epochs+1):
        # initialize variable to monitor training and validation loss
        train_loss = 0.0
        valid_loss = 0.0

        PDNet.train()

        for batch_idx, (data, label) in enumerate(loader['train']):

            data, label = data.to(device), label.to(device ) # move data to device (gpu or cpu)

            #reset gradient accumulation
            optimizer.zero_grad()

            #forward pass
            output = PDNet(data)

            #loss
            loss = criterion(output, label)

            #back propogation pass: calculates the gradient of the loss with respect to model parameters
            loss.backward()

            #updates weights and biases by one step
            optimizer.step()

            train_loss += loss.item() * data.size(0)

    #####################
    # model validation #
    ####################

        PDNet.eval()
        for batch_idx, (data, label) in enumerate(loader['valid']):

            data, label = data.to(device), label.to(device)

            output = PDNet(data)

            loss = criterion(output,label)

            valid_loss += loss.item()*data.size(0)

        train_loss = train_loss / len(loader['train'].dataset)
        valid_loss = valid_loss / len(loader['valid'].dataset)


        # print training and validation statistics

        print(f'Epoch: {epoch} \tTraining Loss: {train_loss} \tValidation Loss: {valid_loss}')

        # Save model if the validation loss has decreased

        if resume_training: ## yes i know this is a bit convoluted right now, but it will do the job for now, if this is a continuation of of training, itll update loaded epoch
            load_epoch += 1
            print(f'Total epochs trained: {load_epoch}')

        if valid_loss <= valid_loss_min:
            print(f'Validation loss decreased ({valid_loss_min} --> {valid_loss}.) Saving model ...\n')

            if resume_training: ## yes i know this is a bit convoluted right now, but it will do the job for now, will save using the load_epoch instead of the epoch
                modelCNN.save_model(load_epoch, PDNet.state_dict(), optimizer.state_dict(), PDNet.fc[6].out_features, PDNet.fc[0].out_features, device, save_load_path)
            else: ## yes i know this is a bit convoluted right now, but it will do the job for now, if its not a continuation training, will save using new epoch
                modelCNN.save_model(epoch, PDNet.state_dict(), optimizer.state_dict(), PDNet.fc[6].out_features, PDNet.fc[0].out_features, device, save_load_path)
            valid_loss_min = valid_loss

    print('Done Training!')

    return PDNet

# def test_model():


if __name__ == '__main__':

    # Train using Cuda GPU if available, if not then use the CPU(not recommended)
    device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')
    print(f'{device} detected, using {device} to train model')

    #uncomment to train new model
    # img_data_root = './data'
    # save_path = './checkpoint_2.pt'
    #
    # train_model(img_data_root, device,15,save_path, lr=0.001, batch_size=32, hidden_units = 4096, resume_training=False)

    #Uncomment to resume training
    img_data_root = './data'
    load_path = './checkpoint_2.pt'

    train_model(img_data_root, device,15,load_path, lr=0.001, batch_size=32, hidden_units = 4096, resume_training=True)
