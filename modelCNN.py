import numpy as np
import matplotlib.pyplot as plt
import json
from PIL import Image

import torch
from torch import nn
from torch import optim
import torchvision
from torchvision import datasets, models, transforms


def load_data(img_data_root, batch_size=64):
    '''
    input_args
        img_data_root: is the root directory of the image containing the training('train'), validation('valid'), and test('test') image directories
        batch_size: size of the batches for training the model

    output

    '''

    # Tranformations for processing and loading train/test data
    process_transform = transforms.Compose([
                        transforms.Resize(256),
                        transforms.CenterCrop(224),
                        transforms.ToTensor(),
                        transforms.Normalize(mean=[0.485, 0.456, 0.406], std=[0.229, 0.224, 0.225])
                        ])
    augment_transform = transforms.Compose([
                        transforms.RandomResizedCrop(224),
                        transforms.RandomHorizontalFlip(),
                        transforms.ToTensor(),
                        transforms.Normalize([0.485, 0.456, 0.406], [0.229, 0.224, 0.225])
                        ])


    train_data = datasets.ImageFolder(img_data_root +'/train/', transform=augment_transform)
    train_loader = torch.utils.data.DataLoader(train_data, batch_size=batch_size, shuffle=True)

    ######## Un comment these four lines if there is a seperate test and validation folders #########
    # valid_data = datasets.ImageFolder(img_data_root +'/valid/', transform=process_transform)
    # valid_loader = torch.utils.data.DataLoader(valid_data, batch_size=batch_size, shuffle=True)
    # test_data = datasets.ImageFolder(img_data_root +'/test/', transform=process_transform)
    # test_loader = torch.utils.data.DataLoader(test_data, batch_size=batch_size, shuffle=True)
    ###############################################################################################

    ### Split test set into test and valid set, comment out the following lines if there is a seperate test and validation folder ###
    test_valid_data = datasets.ImageFolder(img_data_root +'/test/', transform=process_transform)
    valid_len = len(test_valid_data)//2
    test_len = len(test_valid_data) - valid_len

    valid_data, test_data = torch.utils.data.random_split(test_valid_data, [valid_len , test_len])


    valid_loader = torch.utils.data.DataLoader(valid_data, batch_size=batch_size, shuffle=True)
    test_loader = torch.utils.data.DataLoader(test_data, batch_size=batch_size, shuffle=True)
    ##################################################################################################################################

    data = {'train': train_data, 'valid': valid_data, 'test': test_data}
    loader = {'train': train_loader, 'valid': valid_loader, 'test': test_loader}

    print(f'Data Loaded from {img_data_root}, batch size: {batch_size}')
    print(f'# of images in training set: {len(train_data)}')
    print(f'# of images in validation set: {len(valid_data)}')
    print(f'# of images in testing set: {len(test_data)}')

    return data, loader


def model_architecture(num_classes, hidden_units=4096 ):

    model = models.resnet18(pretrained=True)

    for param in model.parameters():
        param.requires_grad=False

    model.fc = nn.Sequential(nn.Linear(model.fc.in_features, hidden_units),
                                       nn.ReLU(),
                                       nn.Dropout(0.2),
                                       nn.Linear(hidden_units, hidden_units//2),
                                       nn.ReLU(),
                                       nn.Dropout(0.2),
                                       nn.Linear(hidden_units//2, num_classes) )

    # print(model)

    return model

def initiate_optimizer_loss(parameters, lr=0.001):

    optimizer = optim.Adam(parameters, lr)

    criterion = nn.CrossEntropyLoss()
    return optimizer, criterion



def save_model(epoch, model_state_dict, optimizer_state_dict, model_num_classes, model_hidden_units, device, save_path):
    ''' '''

    checkpoint = {'epoch': epoch,
                  'model_state_dict' : model_state_dict,
                  'optimizer_state_dict' : optimizer_state_dict,
                  'model_num_classes' : model_num_classes,
                  'model_hidden_units' : model_hidden_units,
                  'device' : device
                  }

    torch.save(checkpoint, save_path)



def load_model(load_path, device='cpu'): #need to change this in the future in order for checkpoint to save lr
    ''' '''
    checkpoint = torch.load(load_path, map_location = device)

    model = model_architecture(checkpoint['model_num_classes'], checkpoint['model_hidden_units'])
    model.load_state_dict(checkpoint['model_state_dict'])

    optimizer, criterion = initiate_optimizer_loss(model.fc.parameters())
    optimizer.load_state_dict(checkpoint['optimizer_state_dict'])

    # maps optimizer paremeters to the device running model
    for state in optimizer.state.values():
        for k, v in state.items():
            if isinstance(v, torch.Tensor):
                state[k] = v.to(device)

    epoch = checkpoint['epoch']

    print('\nModel Loading Successfull\n')
    return model, optimizer, criterion, epoch

def export_classes(data_root): ### this code needs to fixed

    data, loader = load_data(data_root)
    print(data['train'])
    class_names = [item[4:].replace("_", " ") for item in data['train'].classes]

    with open('data_classes.json', 'w') as write_file:
        json.dump(class_names, write_file)

    return class_names

def predictPD(img, model_path, class_path, device='cpu', img_direct= False):

    with open(class_path) as json_file:
        class_names = json.load(json_file)

    model, _, _, _ = load_model(model_path, device)
    model.to(device)

    in_transform = transforms.Compose([
                        transforms.Resize(256),
                        transforms.CenterCrop(224),
                        transforms.ToTensor(),
                        transforms.Normalize(mean=[0.485, 0.456, 0.406], std=[0.229, 0.224, 0.225])
                        ])
    if img_direct:
        image = Image.open(img)
    else:
        image = Image.fromarray(img)
    image = in_transform(image).unsqueeze(0).to(device)

    model.eval()

    out = model(image)

    value, idx = torch.nn.functional.softmax(out[0], dim=0).max(0)



    return class_names[str(idx.cpu().numpy())], idx




################################################## Dev Testing


if __name__ == '__main__':

    device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')
    print(f'{device} detected, using {device} to run model')

    out, idx = predictPD('./test_images/test_image_Cedar_rust.jpg','./checkpoint_2.pt', './imagenet_class_index.json', device, img_direct=True)

    # print(idx)
    print(out)

    # data, loader = load_data('./data', 128)
    #
    # dataiter = iter(loader['train'])
    # images, labels = dataiter.next()
    #
    # print(len(data['train'].classes))
    #
    #
    # model = model_architecture(10,4096)
    # print(model.fc[6].out_features)
    # save_model()
    #
    # test_model= load_model('./test.pt')
    #
    # print(test_model)
