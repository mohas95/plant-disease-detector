from flask import Flask, render_template, redirect, request, url_for
import base64
import numpy as np
import cv2
import io
import modelCNN

import torch
import re
from src import webscrapper
import os
from werkzeug.utils import secure_filename

app = Flask(__name__)


#
UPLOAD_FOLDER = os.path.join(os.path.dirname(__file__),'img')

ALLOWED_EXTENSIONS = {'txt', 'pdf', 'png', 'jpg', 'jpeg', 'gif'}

app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER

def allowed_filename(filename):
    return '.' in filename and \
           filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS

@app.route('/', methods=['GET'])
def hello_world(): 
    if request.method == 'GET':
        print('hello')
        return render_template('index.html')
    print('error')
    return redirect(url_for('error_route'))

@app.route('/',methods=['POST'])
def upload():
    picture= request.files['file']
    picture_bytes = picture.read()
    npimg = np.fromstring(picture_bytes, np.uint8)

    print(picture)

    #img becomes np image we can use for CV model
    img = cv2.imdecode(npimg, cv2.IMREAD_COLOR)
    device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')
    

    if picture and allowed_filename(picture.filename):
        # filename = secure_filename(picture.filename)
        filename= picture.filename
        img_path = os.path.join(app.config['UPLOAD_FOLDER'], filename)
        picture.save(img_path)
        print(f'this is the img_path: {img_path}')
        disease = modelCNN.predictPD(picture.stream, './checkpoint_2.pt','./imagenet_class_index.json',device,img_direct=True)
        print(disease)
        #pass image to predict function here!!!
        disease = disease[0]
        disease = re.sub(' +', ' ', disease)
        disease = disease.split(' ', 1)[1]
        display_info = get_disease_info(disease)

        img = "data:image/jpeg;base64, "+base64.b64encode(picture_bytes).decode('ascii')
        
        return render_template('index.html',picture=img, disease=disease, info=display_info)
    return render_template('index.html')


def get_disease_info(disease):
    return webscrapper.scraper(query=disease)


@app.route('/output')
def run_model():
    return render_template('output.html')
    #method to test on the image

def load_model():
    model = modelCNN.model_architecture(39)
    model = torch.load('./checkpoint_2.pt')
    print(model)
    return model


@app.route('/error')
def error_route():
    return 'error'

if __name__ == "__main__":
    app.run()
